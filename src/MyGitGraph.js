import React from 'react'
import { Gitgraph, Mode, Orientation, TemplateName } from '@gitgraph/react'

const options = {
  orientation: Orientation.Horizontal,
}

const graph = (gitgraph) => {
  // Simulate git commands with Gitgraph API.
  const master = gitgraph.branch('trunk')
  master.commit('Initial commit')

  const develop = master.branch('develop')
  develop.commit('Add TypeScript')

  const aFeature = develop.branch('a-feature')
  aFeature.commit('Make it work').commit('Make it right').commit('Make it fast')

  develop.merge(aFeature)
  develop.commit('Prepare v1')

  master.merge(develop).tag('v1.0.0')
}

export default function MyGitGraph() {
  return <Gitgraph options={options}>{(gitgraph) => graph(gitgraph)}</Gitgraph>
}
