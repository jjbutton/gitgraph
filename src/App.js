import React from 'react'
import logo from './logo.svg'
import './App.css'
import MyGitGraph from './MyGitGraph'

function App() {
  return (
    <div className='App'>
      <MyGitGraph></MyGitGraph>
    </div>
  )
}

export default App
